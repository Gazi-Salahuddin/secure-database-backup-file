# Secure Database Backup file 

We can secure database backup (.DMP) file using Data Pump Backup utilities in oracle 11g or higher database edition.

_Command for Export: _

Need to create a directory in the database as “backup_dir”. 

expdp demo/demo@xe dumpfile=backup_dir:bkp1.dmp logfile=backup_dir:bkup.log 
encryption=all encryption_mode=password encryption_password=test encryption_algorithm=aes256

_Command for Import:_

impdp demo/demo@xe dumpfile=backup_dir:bkp1.dmp logfile=backup_dir:bkup.log encryption_password=test

**Taking data pump backup from the forms end:**

		
	    BEGIN
            Host('expdp demo/demo@xe dumpfile=DATA_PUMP_DIR:ASL-POS_'||to_char(sysdate)||'-'|| '.dmp 
            logfile=DATA_PUMP_DIR:bkup.log encryption=all encryption_mode=password encryption_password=pass@321#
             encryption_algorithm=aes256');

		    Message('You Have Succesfuly Taken a Backup of Your Database');
		    Message('You Have Succesfuly Taken a Backup of Your Database');
        END;

        
*Note: 11g Database is compatible with oracle 6i and above forms and reports developer. 
